<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>The Boris & Nicole Show</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />


    <? //All of our CSS compiled ?>
	<link rel="stylesheet" href="/css/styles.min.css" type="text/css">

	<? //Moderizr & Respond.js ?>
	<script src="/js/header.min.js"></script>
</head>
<body>


<div class="container">
    <div class="row">
		<div class="col-md-6 logo">
            <img src="images/borris-and-nicole.png">

		</div>
        <div class="col-md-6 social">
            <a href="http://www.instagram.com/borisandnicole" target="_blank"><i class="fa fa-instagram"></i></a>
            <a href="http://www.vine.co/BorisandNicole" target="_blank"><i class="fa fa-vine"></i></a>
            <a href="http://www.pinterest.com/borisandnicole" target="_blank"><i class="fa fa-pinterest-p"></i></a>
            <a href="http://www.twitter.com/borisandnicole" target="_blank"><i class="fa fa-twitter"></i></a>
            <a href="http://www.facebook.com/borisandnicole" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="http://www.youtube.com/channel/UCa5K9TmVAmJTFPvbHX7cHBg" target="_blank"><i class="fa fa-youtube-play"></i></a>
        </div>
	</div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 main">
            <img src="images/main-img.jpg">
        </div>
    </div>
</div>
<div class="container-fluid blue-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <p>If you’re going to be in the los angeles area,<br>
                come see a live taping of <strong>THE BORIS & NICOLE SHOW!</strong></p>
            </div>
            <div class="col-md-4">
                <div class="button"><a href="http://on-camera-audiences.com/shows/The_Boris_and_Nicole_Show" target="_blank">GET TICKETS</a></div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid boris">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <img src="images/boris.png">
            </div>
            <div class="col-md-9">
                <div class="video-container">
                    <script>
                        var videos = [
                            'pXA_zpbgaO4?controls=0&amp;showinfo=0',
                            'Vr5GesS29wI?controls=0&amp;showinfo=0',
                            'lbH1K6xsSdc?controls=0&amp;showinfo=0',
                            '9_Ml5piAS90?controls=0&amp;showinfo=0'

                        ];

                        var index=Math.floor(Math.random() * videos.length);
                        var html='<div><iframe width="80%" height="380" src="http://www.youtube.com/embed/' + videos[index] + '" frameborder="0" allowfullscreen></iframe></div>';
                        document.write(html);
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid dark-blue-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="button"><a href="../featured-products">FEATURED PRODUCTS</a></div>
            </div>
            <div class="col-md-8">
                <p>LIKE SOMETHING YOU SAW ON THE SHOW?<br>
                    <a href="../featured-products">CLICK HERE</a> TO SEE THE FEATURED PRODUCTS</p>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid nicole">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h3>WHERE TO WATCH</h3>
                <table>
                    <tr>
                        <td>ALBUQUERQUE</td>
                        <td>FOX 2</td>
                        <td>10am</td>
                    </tr>
                    <tr>
                        <td>ATLANTA</td>
                        <td>FOX 5</td>
                        <td>1pm</td>
                    </tr>
                    <tr>
                        <td>BALTIMORE</td>
                        <td>MYNETWORK 24</td>
                        <td>4pm</td>
                    </tr>
                    <tr>
                        <td>BUFFALO</td>
                        <td>CW 23</td>
                        <td>1pm</td>
                    </tr>
                    <tr>
                        <td>DETROIT</td>
                        <td>FOX 2</td>
                        <td>2pm</td>
                    </tr>
                    <tr>
                        <td>GRAND RAPIDS</td>
                        <td>ABC 41</td>
                        <td>4pm</td>
                    </tr>
                    <tr>
                        <td>HARTFORD</td>
                        <td>MYNETWORK 59</td>
                        <td>9am</td>
                    </tr>
                    <tr>
                        <td>INDIANAPOLIS</td>
                        <td>CW 8</td>
                        <td>10am</td>
                    </tr>
                    <tr>
                        <td>LANSING</td>
                        <td>ABC 53</td>
                        <td>3pm</td>
                    </tr>
                    <tr>
                        <td>LOS ANGELES</td>
                        <td>FOX 11</td>
                        <td>1pm</td>
                    </tr>
                    <tr>
                        <td>LOUISVILLE</td>
                        <td>MYNETWORK 58</td>
                        <td>10am</td>
                    </tr>
                    <tr>
                        <td>MOBILE</td>
                        <td>CW 55</td>
                        <td>TBA</td>
                    </tr>
                    <tr>
                        <td>NEW YORK</td>
                        <td>FOX 5</td>
                        <td>11am</td>
                    </tr>
                    <tr>
                        <td>NORFOLK</td>
                        <td>FOX 43</td>
                        <td>10am</td>
                    </tr>
                    <tr>
                        <td>PHILADELPHIA</td>
                        <td>FOX 29</td>
                        <td>2pm</td>
                    </tr>
                    <tr>
                        <td>PHOENIX</td>
                        <td>FOX 10</td>
                        <td>2pm</td>
                    </tr>
                    <tr>
                        <td>RALEIGH</td>
                        <td>MYNETWORK 28</td>
                        <td>12noon</td>
                    </tr>
                    <tr>
                        <td>SIOUX FALLS</td>
                        <td>MYNETWORK</td>
                        <td>9am</td>
                    </tr>

                    <tr>
                        <td>TOPEKA</td>
                        <td>ABC 49</td>
                        <td>12noon</td>
                    </tr>
                    <tr>
                        <td>WASHINGTON DC</td>
                        <td>FOX 5</td>
                        <td>12noon</td>
                    </tr>
                    <tr>
                        <td>YOUNGSTOWN</td>
                        <td>CBS 27</td>
                        <td>3pm</td>
                    </tr>



                </table>
            </div>
            <div class="col-md-4">
                <img src="images/nicole.png">
            </div>
        </div>
    </div>
</div>
<div class="container-fluid footer">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <img src="../images/footer-logo.png">
            </div>
            <div class="col-md-10 copyright">
                <p>Use of this website assumes acceptance of the <a href="../terms-and-conditions/"><strong>TERMS & CONDITIONS</strong></a> and <a href="../privacy-policy/"><strong>PRIVACY POLICY.</strong></a></p>
                <p><strong>TM and © 2015 Lincolnwood Drive Inc. All rights reserved.</strong></p>
            </div>
        </div>
    </div>
</div>




<script src="/js/app.min.js" type="application/javascript"></script>

<?  //Google Analytics: change UA-XXXXX-X to be your site's ID. ?>
<script>
	(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
		function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
		e=o.createElement(i);r=o.getElementsByTagName(i)[0];
		e.src='https://www.google-analytics.com/analytics.js';
		r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
	ga('create','UA-XXXXX-X','auto');ga('send','pageview');
</script>

</body>
</html>

