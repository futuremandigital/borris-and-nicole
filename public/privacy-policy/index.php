<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Privacy Policy</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="../favicon-16x16.png" sizes="16x16" />

    <? //All of our CSS compiled ?>
    <link rel="stylesheet" href="/css/styles.min.css" type="text/css">

    <? //Moderizr & Respond.js ?>
    <script src="/js/header.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4 logo"><a href="http://www.borisandnicole.com"><img src="../images/borris-and-nicole.png"></a></div>
        <div class="col-md-4"></div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>Privacy Policy</h3>
            <p>Effective Date: September 30, 2015</p>
            <h4>Table of Contents</h4>
            <ol>
                <li><a href="#intro">INTRODUCTION</a></li>
                <li><a href="#collection">COLLECTION OF INFORMATION</a></li>
                <li><a href="#use">USE AND DISCLOSURE</a></li>
                <li><a href="#security">SECURITY</a></li>
                <li><a href="#user-access">USER ACCESS AND CONTROL</a></li>
                <li><a href="#other">OTHER IMPORTANT INFORMATION<a/></li>
                <li><a href="#contact">CONTACT US</a></li>
            </ol>
            <div id="intro"><h4>INTRODUCTION</h4></div>
            <p>Lincolnwood Drive, Inc. (“Company” or “we”) wants you to be familiar with how we collect, use and disclose information from and about you. This Privacy Policy describes our practices in connection with information collected through services such as websites, mobile sites, applications (“apps”), and widgets (collectively, the “Company Services”).  By using Company Services, you agree to the terms and conditions of this Privacy Policy.  A list of our affiliated companies (“Company Affiliates”) is available at <a href="http://www.21cf.com">www.21cf.com.</a></p>
            <p>As set forth in our <a href="http://borisandnicole.com/terms-and-conditions/">Terms of Use</a>, the Company Services are for a general audience, are not targeted to children, and do not knowingly collect personal information from children under 13 years of age.</p>
            <p>This Privacy Policy applies to all users, including both those who use the Company Services without being registered with or subscribing to a Company Service and those who have registered with or subscribed to a Company Service.  This Privacy Policy applies to Company’s collection and use of your personal information (i.e., information that identifies a specific person, such as full name or email address).  It also describes generally Company’s practices for handling non-personal information (for example, interests, demographics and services usage). </p>

            <div id="collection"><h4>COLLECTION OF INFORMATION</h4></div>
            <p>Company and our service providers collect the following information from and about you:</p>
            <p><strong>Registration Information</strong> is the information you submit to register for a Company Service, for example, to create an account, post comments, receive a newsletter, or enter a contest or sweepstakes.  Registration Information may include, for example, name, email address, gender, zip code and birthday. </p>
            <p><strong>Public Information and Posts</strong> consist of comments or content that you post to the Company Services and the information about you that accompanies those posts or content, which may include a name, user name, comments, likes, status, profile information and picture.  Public Information and Posts are always public, which means they are available to everyone and may be displayed in search results on external search engines. </p>
            <p><strong>Information from Social Media.</strong>  If you access or log-in to a Company Service through a social media service or connect a Company Service to a social media service, the information we collect may also include your user ID and/or user name associated with that social media service, any information or content you have permitted the social media service to share with us, such as your profile picture, email address or friends lists, and any information you have made public in connection with that social media service.  When you access the Company Services through social media services or when you connect a Company Service to social media services, you are authorizing Company to collect, store, and use such information and content in accordance with this Privacy Policy.</p>
            <p><strong>Activity Information.</strong> When you access and interact with the Company Services, Company and its service providers may collect certain information about those visits.  For example, in order to permit your connection to the Company Services, our servers receive and record information about your computer, device, and browser, including potentially your IP address, browser type, and other software or hardware information.  If you access the Company Services from a mobile or other device, we may collect a unique device identifier assigned to that device, geolocation data, or other transactional information for that device.</p>
            <p>Cookies and other tracking technologies (such as browser cookies, pixels, beacons, and Adobe Flash technology including cookies) are comprised of small bits of data that often include a de-identified or anonymous unique identifier.  Company’s websites, apps and other services send this data to your browser when you first request a web page and then store the data on your computer or other device so the website or app can access information when you make subsequent requests for pages from that service.  These technologies may also be used to collect and store information about your usage of the Company Services, such as pages you have visited, the video and other content you have viewed, search queries you have run and advertisements you have seen.</p>
            <p>Third parties that support the Company Services by serving advertisements or providing services, such as allowing you to share content or tracking aggregate Company Services usage statistics, may also use these technologies to collect similar information when you use the Company Services or third-party services.  Company does not control these third-party technologies and their use is governed by the privacy policies of third parties using such technologies.  For more information about third-party advertising networks and similar entities that use these technologies, including your choices with respect to them, see the section entitled “To deliver relevant advertisements,” below.
            </p>
            <p>Most browsers are initially set to accept cookies, but you can change your settings to notify you when a cookie is being set or updated, or to block cookies altogether.  Please consult the "Help" section of your browser for more information.  Users can manage the use of Flash technologies, with the Flash management tools available at Adobe’s website, see <a href="http://www.macromedia.com/support/documentation/en/flashplayer/help/settings_manager.html.">http://www.macromedia.com/support/documentation/en/flashplayer/help/settings_manager.html.</a> Please note that by blocking any or all cookies you may not have access to certain features, content or personalization available through the Company Services.</p>
            <p><strong>Information from Other Sources.</strong>  We may supplement the information we collect with information from other sources, such as publicly available information from social media services, commercially available sources and information from our Company Affiliates or business partners. </p>
            <div id="use"><h4>USE AND DISCLOSURE</h4></div>
            <p>We use the information we collect from and about you to provide the Company Services and features to you, including: to measure and improve those Company Services and features; to improve your experience with both online and off-line Company Services by delivering content you will find relevant and interesting, including advertising and marketing messages; to allow you to comment on content, and participate in online games, contests, or rewards programs; to provide you with customer support and to respond to inquiries.  In order to provide you with content and advertising that is more interesting and relevant to you, we may use the information from and about you to make inferences and predictions about your potential areas of interest.  When the information collected from or about you does not identify you as a specific person, we may use that information for any purpose or share it with third parties.</p>
            <p>We use the information we collect from and about you for these additional purposes:</p>
            <p><strong>To allow service providers to assist us in providing and managing the Company Services.</strong>  The information we collect from and about you may be made available to certain third party service providers, such as contractors, analytics and other measurement companies, agents or sponsors, who help us analyze and understand your use of the Company Services, and manage and/or provide the Company Services.  </p>
            <p><strong>To allow social sharing functionality.</strong> If you log in with or connect a social media service account with Company Services, we may share your user name, picture, and likes, as well as your activities and comments with other Company Services users and with your friends associated with your social media service. We may also share the same information with the social media service provider. By logging in with or connecting your Company Services account with a social media service, you are authorizing us to share information we collect from and about you with the social media service provider, other users and your friends and you understand that the social media service’s use of the shared information will be governed by the social media service’s privacy policy.  If you do not want your personal information shared in this way, please do not connect your social media service account with your Company Services account and do not participate in social sharing on Company Services.</p>
            <p><strong>To provide co-branded services and features.</strong>  We may offer co-branded services or features, such as contests, sweepstakes or other promotions together with a third party (“Co-Branded Services”).  These Co-Branded Services may be hosted on Company Services or on the third party’s service.  By virtue of these relationships, we may share the information you submit in connection with the Co-Branded Service with the third party.  The third party’s use of your information will be governed by the third party’s privacy policy. </p>
            <p><strong>To deliver relevant advertisements.</strong>  Company and our service providers may use the information we collect from and about you to deliver relevant ads to you when you use the Company Services or another service.  As noted above, third-party advertising networks and advertisers may use cookies and similar technologies to collect and track information such as demographic information, inferred interests, aggregated information, and activity to assist them in delivering advertising that is more relevant to your interests.  To find out more about third-party advertising networks and similar entities that use these technologies, see <a href="www.aboutads.info">www.aboutads.info.</a>  If you would like to opt-out of such ad networks’ and services’ advertising practices, go to <a href="www.aboutads.info/choices">www.aboutads.info/choices</a> to opt-out in desktop and mobile web browsers.  You may download the AppChoices app at <a href="www.aboutads.info/appchoices">www.aboutads.info/appchoices</a? to opt-out in mobile apps.</p>
            <p><strong>To contact you.</strong>  Company may periodically send promotional materials or notifications related to the Company Services.  If you want to stop receiving promotional materials, you can either send an email to <a href="mailto:privacy-scpi@foxtv.com">privacy-scpi@foxtv.com</a> or follow the unsubscribe instructions at the bottom of each email.  There are certain service notification emails that you may not opt-out of, such as notifications of changes to the Company Services or policies. If you have installed a mobile app and you wish to stop receiving push notifications, you can change the settings either on your mobile device or through the app.</p>
            <p><strong>To share with our Company Affiliates.</strong>  Company may share your information with Company Affiliates for the purposes described in this Privacy Policy.  A link to a website that lists Company Affiliates can be found at the beginning of this policy.  Users who visit Company Affiliates’ services should still refer to their separate privacy policies, which may differ in some respects from this Privacy Policy. </p>
            <p><strong>To share with business partners.</strong>  Company may share your information with business partners to permit them to send you marketing communications consistent with your choices. </p>
            <p><strong>To protect the rights of Company and others.</strong>  There may be instances when Company may use or disclose your information, including situations where Company has a good faith belief that such use or disclosure is necessary in order to: (i) protect, enforce, or defend the legal rights, privacy, safety, or property of Company, our Company Affiliates or their employees, agents and contractors (including enforcement of our agreements and our terms of use); (ii) protect the safety, privacy, and security of users of the Company Services or members of the public; (iii) protect against fraud or for risk management purposes; (iv) comply with the law or legal process; or (v) respond to requests from public and government authorities.</p>
            <p><strong>To complete a merger or sale of assets.</strong>  If Company sells all or part of its business or makes a sale or transfer of its assets or is otherwise involved in a merger or transfer of all or a material part of its business (including in connection with a bankruptcy or similar proceedings), Company may transfer your information to the party or parties involved in the transaction as part of that transaction.</p>
            <div id="security"><h4>SECURITY</h4></div>
            <p>Company uses commercially reasonable administrative, technical, personnel and physical measures to safeguard information in its possession against loss, theft and unauthorized use, disclosure or modification. However, no one can guarantee the complete safety of your information. </p>
            <div id="user-access"><h4>USER ACCESS AND CONTROL</h4></div>
            <p>If you would like to access, review, correct, update, suppress, or otherwise limit our use of your personal information you have previously provided directly to us, you may contact us as outlined in Section 3; or using the mechanisms provided below. In your request, please include your email address, name, address, and telephone number and specify clearly what information you would like to access, change, update, or suppress.  We will try to comply with your request as soon as reasonably practicable. </p>
            <p>If you are a California resident, you may request that we not share your personal information on a going-forward basis with Company Affiliates or unaffiliated third parties for their direct marketing purposes by contacting us as outlined in Section 3 and putting “shine the light opt-out” in the subject line and your full name, email address and postal address in the body of the email.   We will try to comply with your request(s) as soon as reasonably practicable. </p>
            <div id="other"><h4>OTHER IMPORTANT INFORMATION</h4></div>
            <p><strong>Updates to Privacy Policy.</strong>  Company may modify this Privacy Policy.  Please look at the Effective Date at the top of this Privacy Policy to see when this Privacy Policy was last revised.  Any changes to this Privacy Policy will become effective when we post the revised Privacy Policy on the Company Services.  </p>
            <p><strong>Location of Data.</strong>  The Company Services are hosted in and managed from the United States.  If you are a user located outside the United States, you understand and consent to having any personal information processed in the United States.  United States data protection and other relevant laws may not be the same as those in your jurisdiction.  This includes the use of cookies and other tracking technologies as described above.  As a result, please read this Privacy Policy with care.</p>
            <p><strong>Linked Services.</strong>  The Company Services may also be linked to sites operated by unaffiliated companies, and may carry advertisements or offer content, functionality, games, newsletters, contests or sweepstakes, or applications developed and maintained by unaffiliated companies. Company is not responsible for the privacy practices of unaffiliated companies, and once you leave the Company Services or click an advertisement you should check the applicable privacy policy of the other service.  </p>
            <p>In addition, Company is not responsible for the privacy or data security practices of other organizations, such as Facebook, Tumblr, Twitter, Apple, Google, Microsoft or any other app developer, app provider, social media platform provider, operating system provider, wireless service provider, or device manufacturer, including in connection with any information you disclose to other organizations through or in connection with the Company Services.</p>
            <p><strong>Collection of Personal Financial Information by a Payment Service.</strong> In some cases, we may use an unaffiliated payment service to allow you to purchase a product or make payments (“Payment Service”).  If you wish to purchase a product or make a payment using a Payment Service, you will be directed to a Payment Service webpage.  Any information that you provide to a Payment Service will be subject to the applicable Payment Service's privacy policy, rather than this Privacy Policy.  We have no control over, and are not responsible for, any Payment Service's use of information collected through any Payment Service.  </p>
            <p><strong>Data Retention.</strong>  We will retain your information for the period necessary to fulfill the purposes outlined in this Privacy Policy unless a longer retention period is required or allowed by law.  </p>
            <p>Remember that even after you cancel your account, copies of some information from your account may remain viewable in some circumstances where, for example, you have shared information with social media or other services.  Because of the nature of caching technology, your account may not be instantly inaccessible to others.  We may also retain backup information related to your account on our servers for some time after cancelation for fraud detection or to comply with applicable law or our internal security policies.  It is not always possible to completely remove or delete all of your information due to technical constraints, contractual, financial or legal requirements.</p>
            <p><strong>Sensitive Information.</strong>  We ask that you not send us, and you not disclose, any sensitive personal information (such as social security numbers, information related to racial or ethnic origin, political opinions, religion or other beliefs, health, criminal background or trade union membership) on or through the Company Services or otherwise.</p>
            <div id="contact"><h4>CONTACT US</h4></div>
            <p>If you have questions about this Privacy Policy, please contact us:<br>
                Email:  privacy-scpi@foxtv.com<br>
                By mail: Lincolnwood Drive, Inc., 1999 S. Bundy Drive, 3rd Floor, Los Angeles, CA 90025, Attn: Stephen Brown/Production<br>
                By telephone: (310) 584-3280<br>
                By fax: (310) 584-2258
            </p>
        </div>

    </div>
</div>

<div class="container-fluid footer">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <img src="../images/footer-logo.png">
            </div>
            <div class="col-md-10 copyright">
                <p>Use of this website assumes acceptance of the <a href="../terms-and-conditions/"><strong>TERMS & CONDITIONS</strong></a> and <a href="../privacy-policy/"><strong>PRIVACY POLICY.</strong></a></p>
                <p><strong>TM and © 2015 Lincolnwood Drive Inc. All rights reserved.</strong></p>
            </div>
        </div>
    </div>
</div>




<script src="/js/app.min.js" type="application/javascript"></script>

<?  //Google Analytics: change UA-XXXXX-X to be your site's ID. ?>
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='https://www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X','auto');ga('send','pageview');
</script>

</body>
</html>