<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>The Boris & Nicole Show</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="../favicon-16x16.png" sizes="16x16" />


    <? //All of our CSS compiled ?>
    <link rel="stylesheet" href="/css/styles.min.css" type="text/css">

    <? //Moderizr & Respond.js ?>
    <script src="/js/header.min.js"></script>
</head>
<body>

<section id="featured-header">
    <div class="container">
        <div class="row">
            <div class="col-md-3 logo">
                <a href="http://www.borisandnicole.com"><img src="../images/borris-and-nicole.png"></a>

            </div>
            <div class="col-md-9 featured-social">
                <a href="http://www.instagram.com/borisandnicole" target="_blank"><i class="fa fa-instagram"></i></a>
                <a href="http://www.vine.co/BorisandNicole" target="_blank"><i class="fa fa-vine"></i></a>
                <a href="http://www.pinterest.com/borisandnicole" target="_blank"><i class="fa fa-pinterest-p"></i></a>
                <a href="http://www.twitter.com/borisandnicole" target="_blank"><i class="fa fa-twitter"></i></a>
                <a href="http://www.facebook.com/borisandnicole" target="_blank"><i class="fa fa-facebook"></i></a>
                <a href="http://www.youtube.com/channel/UCa5K9TmVAmJTFPvbHX7cHBg" target="_blank"><i class="fa fa-youtube-play"></i></a>
            </div>
        </div>
    </div>
</section>

<div class="container-fluid dark-blue-bar pets">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="button"><a href="../pets">FEATURED BRANDS &<br> DOGS FOR ADOPTION</a></div>
            </div>
            <div class="col-md-8">
                <p>LIKE SOMETHING YOU SAW ON THE PET SEGMENT?<br>
                    <a href="../pets">CLICK HERE</a> FOR THE FEATURED BRANDS AND DOGS UP FOR ADOPTION</p>
            </div>
        </div>
    </div>
</div>

<section id="featured-products">




    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h3>FEATURED PRODUCTS</h3>
            </div>
        </div>
    </div>

    <div class="container">

        <div class="row">
            <div class="col-md-4 text-center product">
                <a href="http://www.Fabfitfun.com" target="_blank">
                <img src="../images/fabfitfun.jpg">
                <h4>FabFitFun</h4>
                </a>
                <p>Type in code BandN for $10 off your box</p>
            </div>
            <div class="col-md-4 text-center product">
                <a href="http://www.Beverlydiamonds.com" target="_blank">
                <img src="../images/beverly-diamonds.jpg">
                <h4>Beverly Diamonds</h4>
                </a>
            </div>
            <div class="col-md-4 text-center product">
                <a href="http://www.burnetieshoes.com/" target="_blank">
                <img src="../images/burnetie-shoes.jpg">
                <h4>Burnetie Shoes</h4>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 text-center product">
                <a href="http://destinycandle.com/" target="_blank">
                    <img src="../images/destiny-candles.jpg">
                    <h4>Destiny Candle</h4>
                </a>
                <p>Coupon code: destiny20</p>
            </div>
            <div class="col-md-4 text-center product">
                <a href="http://www.xtremestep.com" target="_blank">
                <img src="../images/xtreme-step.jpg">
                <h4>Xtreme Step</h4>
                </a>
                <p class="text-left">Click on Store in Menu<br>
                    Click on Izetts Xtreme Step Workout<br>
                    Click on <strong>Add to Cart</strong><br>
                    Click on <strong>Checkout</strong><br>
                    A field to add coupon will display in lower left of the form<br>
                    Enter <strong>B&N06292015</strong> in the coupon field and click <strong>Apply</strong><br>
                    Complete the check out process</p>
            </div>
            <div class="col-md-4 text-center product">
                <a href="http://goo.gl/b64NUz" target="_blank">
                    <img src="../images/classic-wand.jpg">
                    <h4>Classic Wand 25MM</h4>
                </a>
                    <p>Coupon code: BNSHOW</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 text-center product">
                <a href="http://www.courtneysixx.com" target="_blank">
                    <img src="../images/how-2-girl.jpg">
                    <h4>The How 2 Girl</h4>
                </a>
            </div>
            <div class="col-md-4 text-center product">
                <a href="http://www.slimchillers.com" target="_blank">
                    <img src="../images/slim-chillers.jpg">
                    <h4>Slim Chillers</h4>
                </a>
            </div>
            <div class="col-md-4 text-center product">
                <a href="http://www.oldnavy.com" target="_blank">
                    <img src="../images/oldnavy.jpg">
                    <h4>Old Navy</h4>
                </a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 text-center product">
                <a href="http://www.trytheworld.com" target="_blank">
                    <img src="../images/trytheworld.gif">
                    <h4>Try The World</h4>
                </a>
            </div>
            <div class="col-md-4 text-center product">
                <a href="http://www.undercanvasgroup.com" target="_blank">
                    <img src="../images/undercanvas.gif">
                    <h4>Under Canvas</h4>
                </a>
            </div>
            <div class="col-md-4 text-center product">
                <a href="http://www.visitantiguabarbuda.com/" target="_blank">
                    <img src="../images/antigua.gif">
                    <h4>Antigua & Barbuda</h4>
                </a>
            </div>
        </div>


        <div class="row">
            <div class="col-md-4 text-center product">
                <a href="http://store.saveyourdo.com/narrow-band-gymwrap-p2.aspx" target="_blank">
                    <img src="../images/gymwrap.gif">
                    <h4>Save Your Do</h4>
                </a>
                    <p class="text-left">To redeem your 20% discount on Save Your Do Wrap type in code <b>BN20</b></p>

            </div>
            <div class="col-md-4 text-center product">
                <a href="http://stlucianow.com/" target="_blank">
                    <img src="../images/stlucia.jpg">
                    <h4>Saint Lucia</h4>
                </a>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12 text-center">
                <h3>SPECIAL THANKS</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 text-center product">
                <a href="http://www.jacksonlimo.com/" target="_blank">
                    <img src="../images/jackson-limo.jpg">
                    <h4>Jackson Limousine</h4>
                </a>
            </div>
            <div class="col-md-4 text-center product">
                <a href="https://www.eddiezaratsian.com/" target="_blank">
                    <img src="../images/eddie-zaratsian.jpg">
                    <h4>Eddie Zaratsian</h4>
                </a>
            </div>
        </div>
    </div>

</section>







<div class="container-fluid footer">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <img src="../images/footer-logo.png">
            </div>
            <div class="col-md-10 copyright">
                <p>Use of this website assumes acceptance of the <a href="../terms-and-conditions/"><strong>TERMS & CONDITIONS</strong></a> and <a href="../privacy-policy/"><strong>PRIVACY POLICY.</strong></a></p>
                <p><strong>TM and © 2015 Lincolnwood Drive Inc. All rights reserved.</strong></p>
            </div>
        </div>
    </div>
</div>




<script src="/js/app.min.js" type="application/javascript"></script>

<?  //Google Analytics: change UA-XXXXX-X to be your site's ID. ?>
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='https://www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X','auto');ga('send','pageview');
</script>

</body>
</html>

