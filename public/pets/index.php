<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>The Boris & Nicole Show</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="../favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="../favicon-16x16.png" sizes="16x16" />


    <? //All of our CSS compiled ?>
    <link rel="stylesheet" href="/css/styles.min.css" type="text/css">

    <? //Moderizr & Respond.js ?>
    <script src="/js/header.min.js"></script>
</head>
<body>

<section id="featured-header">
    <div class="container">
        <div class="row">
            <div class="col-md-3 logo">
                <a href="http://www.borisandnicole.com"><img src="../images/borris-and-nicole.png"></a>

            </div>
            <div class="col-md-9 featured-social">
                <a href="http://www.instagram.com/borisandnicole" target="_blank"><i class="fa fa-instagram"></i></a>
                <a href="http://www.vine.co/BorisandNicole" target="_blank"><i class="fa fa-vine"></i></a>
                <a href="http://www.pinterest.com/borisandnicole" target="_blank"><i class="fa fa-pinterest-p"></i></a>
                <a href="http://www.twitter.com/borisandnicole" target="_blank"><i class="fa fa-twitter"></i></a>
                <a href="http://www.facebook.com/borisandnicole" target="_blank"><i class="fa fa-facebook"></i></a>
                <a href="http://www.youtube.com/channel/UCa5K9TmVAmJTFPvbHX7cHBg" target="_blank"><i class="fa fa-youtube-play"></i></a>
            </div>
        </div>
    </div>
</section>

<section id="featured-products">




    <div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <h3>FEATURED BRANDS<br>
            <small>ON THE PET SEGMENT</small>
            </h3>
        </div>
    </div>
    </div>

    <div class="container">

      <? require_once('pets.php'); ?>

    </div>


    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h3>DOGS UP FOR ADOPTION</h3>
            </div>
        </div>
    </div>

    <div class="container">
        <? require_once('adoption.php'); ?>
    </div>

</section>







<div class="container-fluid footer">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <img src="../images/footer-logo.png">
            </div>
            <div class="col-md-10 copyright">
                <p>Use of this website assumes acceptance of the <a href="../terms-and-conditions/"><strong>TERMS & CONDITIONS</strong></a> and <a href="../privacy-policy/"><strong>PRIVACY POLICY.</strong></a></p>
                <p><strong>TM and © 2015 Lincolnwood Drive Inc. All rights reserved.</strong></p>
            </div>
        </div>
    </div>
</div>




<script src="/js/app.min.js" type="application/javascript"></script>

<?  //Google Analytics: change UA-XXXXX-X to be your site's ID. ?>
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='https://www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X','auto');ga('send','pageview');
</script>

</body>
</html>

