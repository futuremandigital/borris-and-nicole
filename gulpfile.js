var gulp = require('gulp'),
    gutil = require('gulp-util'),
    less = require('gulp-less'),
    autoprefixer = require('gulp-autoprefixer'),
    minify_css = require('gulp-minify-css'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    source_maps  = require('gulp-sourcemaps');

var autoPrefix_list = ['last 2 version'];

gulp.task('compile-less', function() {
    //This is the file that will contain all imports
    gulp.src('./assets/less/styles.less')
        //get source maps ready
        .pipe(source_maps.init())
        .pipe(less())
        .pipe(autoprefixer({
            browsers: autoPrefix_list,
            cascade:  true
        }))
        .pipe(concat('styles.min.css'))
        .pipe(minify_css())
        .pipe(source_maps.write('/maps'))
        .pipe(gulp.dest('./public/css/'));
});

gulp.task('compile-js', function() {

    return gulp.src([
        //jQuery
        './bower_components/jquery/dist/jquery.js',
        //Bootstrap
        './bower_components/bootstrap/dist/js/bootstrap.min.js',
        //GreenSock Animation Platform
        './bower_components/gsap/src/minified/TweenLite.min.js',
        './bower_components/gsap/src/minified/jquery.gsap.min.js',
        './bower_components/gsap/src/minified/plugins/CSSPlugin.min.js',
        './bower_components/gsap/src/minified/plugins/ScrollToPlugin.min.js',
        './bower_components/gsap/src/minified/easing/EasePack.min.js',
        //Additional JS in assets/js folder
        './assets/js/**/*.js'
        ])
        .pipe(source_maps.init())
        //Combile all the JS together, this will be the name of the file
        .pipe(concat('app.min.js'))
        //Catch errors
        .on('error', gutil.log)
        //Compress the javascript
        .pipe(uglify())
        .pipe(source_maps.write('/maps'))
        .pipe(gulp.dest( './public/js/'));
});

gulp.task('compile-header-js', function() {
    return gulp.src([
        './bower_components/modernizr/modernizr.js/',
        './bower_components/respond/src/respond.js'
    ])
    //Combile all the JS together, this will be the name of the file
    .pipe(concat('header.min.js'))
    //Catch errors
    .on('error', gutil.log)
    //Compress the javascript
    .pipe(uglify())
    .pipe(gulp.dest( './public/js/'));

})



gulp.task('copy-fonts', function() {
    gulp.src([
        './bower_components/fontawesome/fonts/**/*',
        './bower_components/bootstrap/fonts/**/*'
    ])
        .pipe(gulp.dest('./public/fonts'))
});

gulp.task('watch', function() {
    gulp.watch('./assets/less/**/*.less', ['compile-less']);
    gulp.watch('./assets/js/**/*.js', ['compile-js']);
});

gulp.task('default', [
    'compile-less',
    'compile-js',
    'compile-header-js',
    'watch',
    'copy-fonts']);
